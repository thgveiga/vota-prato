#coding:utf-8

class Prato < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_accessible :nome

  validates :nome , :presence=>{:message=>"deve ser preenchido"}, :uniqueness=>{:message=>"nome já cadastrado"}
  #validate :validate_presence_of_more_than_one_restaurante

  has_and_belongs_to_many :restaurantes
  has_one :receita
  
  private
  def  validate_presence_of_more_than_one_restaurante
    erros.add("restaurantes", "deve haver ao menos um restaurante") if restaurantes.empty?

  end 
  
end
