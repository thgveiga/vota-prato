#coding:utf-8

# == Schema Information
#
# Table name: restaurantes
#
#  id            :integer          not null, primary key
#  nome          :string(80)
#  endereco      :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  especialidade :string(255) 


class Restaurante < ActiveRecord::Base

  attr_accessible :nome, :endereco, :especialidade

  validates :nome, :presence => {:message=>"deve ser preenchido"}, :uniqueness => {:message => "nome já cadastrado", :scope => "nome"} , :format => {:with => /^[A-Z].*/}

  validates :endereco, :presence => {:message=>"deve ser preenchido"}, :uniqueness => {:message => "endereco já cadastrado", :scope => "endereco"} 

  validates :especialidade, :presence => {:message=>"deve ser preenchido"}

  has_many :qualificacoes
  has_and_belongs_to_many :pratos

  # accepts_nested_attributes_for :qualificacoes

end
