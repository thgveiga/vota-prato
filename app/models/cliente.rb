#coding:utf-8

class Cliente < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_accessible :nome, :idade

  validates :nome , :presence=>{:message=>"deve ser preenchido"}, :uniqueness=>{:message=>"nome já cadastrado"}
  
  validates_numericality_of :idade, greater_than: 0, less_than:100, message: "deve ser um numero entre 0 e 100"

  has_many :qualificacoes

end
