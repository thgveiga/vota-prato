#coding:utf-8

class Receita < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_accessible :prato_id, :conteudo

  validates :conteudo , :presence=>{:message=>"deve ser preenchido"}
 
  belongs_to :prato

  validates_presence_of :prato_id
  validates_associated :prato
end
