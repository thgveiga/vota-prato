class ClientesController < ApplicationController
  def index 
   if not params[:campo].present?
     @clientes = Cliente.order("nome") 
   else
     @clientes = Cliente.order("#{params[:campo]} #{params[:order]}")
     
     @order = params[:order] if params[:order].present?
     @order = @order == "ASC" ? "DESC" : "ASC"
   end
  end

  
  def show 
    @cliente = Cliente.find(params[:id])
  end

  def new
    @cliente = Cliente.new
  end 

  def create
    @cliente = Cliente.new(params[:cliente])
    if @cliente.save
      redirect_to(action: "show", id: @cliente) 
      # redirect_to clientes_url
    else
      render action: "new"
    end
  end

  def edit
    @cliente = Cliente.find(params[:id])
  end

  def update
    @cliente = Cliente.find(params[:id])
    if @cliente.update_attributes(params[:cliente])
      redirect_to(action: "show", id: @cliente)
    else
      render action: "edit"
    end
  end

  def destroy
    @cliente = Cliente.find(params[:id])
    @cliente.destroy

    redirect_to(action: "index")
  end
end
