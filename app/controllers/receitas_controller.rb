class ReceitasController < ApplicationController
  def index 
   if not params[:campo].present?
     @receitas = Receita.order("conteudo") 
   else
     @receitas = Receita.order("#{params[:campo]} #{params[:order]}")
     
     @order = params[:order] if params[:order].present?
     @order = @order == "ASC" ? "DESC" : "ASC"
   end
  end

  
  def show 
    @receita = Receita.find(params[:id])
  end

  def new
    @receita = Receita.new
  end 

  def create
    @receita = Receita.new(params[:receita])
    if @receita.save
      #redirect_to(action: "show", id: @receita) 
      redirect_to(action: "index")
    else
      render action: "new"
    end
  end

  def edit
    @receita = Receita.find(params[:id])
  end

  def update
    @receita = Receita.find(params[:id])
    if @receita.update_attributes(params[:receita])
      redirect_to(action: "show", id: @receita)
    else
      render action: "edit"
    end
  end

  def destroy
    @receita = Receita.find(params[:id])
    @receita.destroy

    redirect_to(action: "index")
  end
end
