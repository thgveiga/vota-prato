class PratosController < ApplicationController
  def index 
   if not params[:campo].present?
     @pratos = Prato.order("nome") 
   else
     @pratos = Prato.order("#{params[:campo]} #{params[:order]}")
     
     @order = params[:order] if params[:order].present?
     @order = @order == "ASC" ? "DESC" : "ASC"
   end
  end

  
  def show 
    @prato = Prato.find(params[:id])
  end

  def new
    @prato = Prato.new
  end 

  def create
    @prato = Prato.new(params[:prato])
    if @prato.save
      #redirect_to(action: "show", id: @prato) 
      redirect_to(action: "index")
    else
      render action: "new"
    end
  end

  def edit
    @prato = Prato.find(params[:id])
  end

  def update
    @prato = Prato.find(params[:id])
    if @prato.update_attributes(params[:prato])
      redirect_to(action: "show", id: @prato)
    else
      render action: "edit"
    end
  end

  def destroy
    @prato = Prato.find(params[:id])
    @prato.destroy

    redirect_to(action: "index")
  end
end
