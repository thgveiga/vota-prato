class RestaurantesController < ApplicationController
  def index 
   if not params[:campo].present?
     @restaurantes = Restaurante.order("nome") 
   else
     @restaurantes = Restaurante.order("#{params[:campo]} #{params[:order]}")
     
     @order = params[:order] if params[:order].present?
     @order = @order == "ASC" ? "DESC" : "ASC"
   end
  end

  
  def show 
    @restaurante = Restaurante.find(params[:id])
  end

  def new
    @restaurante = Restaurante.new
    # @restaurante.qualificacoes.build
  end 

  def create
    @restaurante = Restaurante.new(params[:restaurante])
    if @restaurante.save
      #redirect_to(action: "show", id: @restaurante) 
      redirect_to(action: "index")
    else
      render action: "new"
    end
  end

  def edit
    @restaurante = Restaurante.find(params[:id])
    # @restaurante.qualificacoes.build
  end

  def update
    @restaurante = Restaurante.find(params[:id])
    if @restaurante.update_attributes(params[:restaurante])
      redirect_to(action: "show", id: @restaurante)
    else
      render action: "edit"
    end
  end

  def destroy
    @restaurante = Restaurante.find(params[:id])
    @restaurante.destroy

    redirect_to(action: "index")
  end

end
