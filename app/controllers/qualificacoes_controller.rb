class QualificacoesController < ApplicationController
  def index 
   if not params[:campo].present?
     @qualificacoes = Qualificacao.order("nota") 
   else
     @qualificacoes = Qualificacao.order("#{params[:campo]} #{params[:order]}")
     
     @order = params[:order] if params[:order].present?
     @order = @order == "ASC" ? "DESC" : "ASC"
   end
  end

  
  def show 
    @qualificacao = Qualificacao.find(params[:id])
  end

  def new
    @qualificacao = Qualificacao.new
  end 

  def create
    @qualificacao = Qualificacao.new(params[:qualificacao])
    if @qualificacao.save
      #redirect_to(action: "show", id: @qualificacao) 
      redirect_to(action: "index")
    else
      render action: "new"
    end
  end

  def edit
    @qualificacao = Qualificacao.find(params[:id])
  end

  def update
    @qualificacao = Qualificacao.find(params[:id])
    if @qualificacao.update_attributes(params[:qualificacao])
      redirect_to(action: "show", id: @qualificacao)
    else
      render action: "edit"
    end
  end

  def destroy
    @qualificacao = Qualificacao.find(params[:id])
    @qualificacao.destroy

    redirect_to(action: "index")
  end
end
