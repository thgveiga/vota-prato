class AddIndexesToQualificacoes < ActiveRecord::Migration
  def change
    add_index(:qualificacoes, :cliente_id)
    add_index(:qualificacoes, :restaurante_id)
  end
end
